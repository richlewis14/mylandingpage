/***************************************************
	CENTER CONTENT
***************************************************/

$(function() {
    $(window).on('resize', function resize()  {
        $(window).off('resize', resize);
        setTimeout(function () {
            var content = $('#content');
            var top = (window.innerHeight - content.height()) / 2;
            content.css('top', Math.max(0, top) + 'px');
            $(window).on('resize', resize);
        }, 50);
    }).resize();
});

/***************************************************
	PRELOADER
***************************************************/

(function(a){var b=new Array,c=new Array,d=function(){},e=0;var f={splashVPos:"35%",loaderVPos:"75%",
splashID:"#jpreContent",showSplash:true,showPercentage:true,autoClose:true,closeBtnText:"Start!",
onetimeLoad:false,debugMode:false,splashFunction:function(){}};var g=function(){if(f.onetimeLoad){var a=document.cookie.split("; ");for(var b=0,c;c=a[b]&&a[b].split("=");b++){if(c.shift()==="jpreLoader")
{return c.join("=")}}return false}else{return false}};var h=function(a){if(f.onetimeLoad){var b=new Date;b.setDate(b.getDate()+a);var c=a==null?"":"expires="+b.toUTCString();
document.cookie="jpreLoader=loaded; "+c}};var i=function(){jOverlay=a("<div></div>").attr("id","jpreOverlay").css({position:"fixed",top:0,left:0,width:"100%",height:"100%",zIndex:9999999})
.appendTo("body");if(f.showSplash){jContent=a("<div></div>").attr("id","jpreSlide").appendTo(jOverlay);var b=a(window).width()-a(jContent).width();
a(jContent).css({position:"absolute",top:f.splashVPos,left:Math.round(50/a(window).width()*b)+"%"});a(jContent).html(a(f.splashID).wrap("<div/>").parent().html());a(f.splashID).remove();
f.splashFunction()}jLoader=a("<div></div>").attr("id","jpreLoader").appendTo(jOverlay);var c=a(window).width()-a(jLoader).width();
a(jLoader).css({position:"absolute",top:f.loaderVPos,left:Math.round(50/a(window).width()*c)+"%"});jBar=a("<div></div>").attr("id","jpreBar").css({width:"0%",height:"100%"}).appendTo(jLoader);
if(f.showPercentage){jPer=a("<div></div>").attr("id","jprePercentage").css({position:"relative",height:"100%"}).appendTo(jLoader).html("")}if(!f.autoclose){jButton=a("<div></div>")
.attr("id","jpreButton").on("click",function(){n()}).css({position:"relative",height:"100%"}).appendTo(jLoader).text(f.closeBtnText).hide()}};var j=function(c){a(c)
.find("*:not(script)").each(function(){var c="";if(a(this).css("background-image").indexOf("none")==-1&&a(this).css("background-image").indexOf("-gradient")==-1){c=a(this)
.css("background-image");if(c.indexOf("url")!=-1){var d=c.match(/url\((.*?)\)/);c=d[1].replace(/\"/g,"")}}else if(a(this).get(0)
.nodeName.toLowerCase()=="img"&&typeof a(this).attr("src")!="undefined"){c=a(this).attr("src")}if(c.length>0){b.push(c)}})};var k=function(){for(var a=0;a<b.length;a++){if(l(b[a]));}};
var l=function(b){var d=new Image;a(d).load(function(){m()}).error(function(){c.push(a(this).attr("src"));m()}).attr("src",b)};
var m=function(){e++;var c=Math.round(e/b.length*100);a(jBar).stop().animate({width:c+"%"},500,"linear");if(f.showPercentage){a(jPer).text(c)}if(e>=b.length){e=b.length;
h();if(f.showPercentage){a(jPer).text("100")}if(f.debugMode){var d=o()}a(jBar).stop().animate({width:"100%"},500,"linear",function(){if(f.autoClose)n();else a(jButton).fadeIn(1e3)})}};
var n=function(){a(jOverlay).fadeOut(800,function(){a(jOverlay).remove();d()})};var o=function(){if(c.length>0)
{var a="ERROR - IMAGE FILES MISSING!!!\n\r";a+=c.length+" image files cound not be found. \n\r";a+="Please check your image paths and filenames:\n\r";
for(var b=0;b<c.length;b++){a+="- "+c[b]+"\n\r"}return true}else{return false}};a.fn.jpreLoader=function(b,c){if(b){a.extend(f,b)}if(typeof c=="function"){d=c}a("body").css({display:"block"});
return this.each(function(){if(!g()){i();j(this);k()}else{a(f.splashID).remove();d()}})}})(jQuery)

/***************************************************
	PRELOAD INIT
***************************************************/
$(document).ready(function(){
	$('body').jpreLoader({
		splashID: "#jSplash",
		showSplash: true,
		showPercentage: true,
		autoClose: true,
		splashFunction: function() {
			$('#circle').delay(250).animate({'opacity' : 1}, 500, 'linear');
		}
	});	
});


/***************************************************
	MODAL
***************************************************/
!function(e){"use strict";var t=function(e,t){this.init(e,t)};t.prototype={constructor:t,init:function(t,n){var r=this;this.options=n;this.$element=e(t).delegate('[data-dismiss="modal"]',"click.dismiss.modal",e.proxy(this.hide,this));this.options.remote&&this.$element.find(".modal-body").load(this.options.remote,function(){var t=e.Event("loaded");r.$element.trigger(t)});var i=typeof this.options.manager==="function"?this.options.manager.call(this):this.options.manager;i=i.appendModal?i:e(i).modalmanager().data("modalmanager");i.appendModal(this)},toggle:function(){return this[!this.isShown?"show":"hide"]()},show:function(){var t=e.Event("show");if(this.isShown)return;this.$element.trigger(t);if(t.isDefaultPrevented())return;this.escape();this.tab();this.options.loading&&this.loading()},hide:function(t){t&&t.preventDefault();t=e.Event("hide");this.$element.trigger(t);if(!this.isShown||t.isDefaultPrevented())return this.isShown=false;this.isShown=false;this.escape();this.tab();this.isLoading&&this.loading();e(document).off("focusin.modal");this.$element.removeClass("in").removeClass("animated").removeClass(this.options.attentionAnimation).removeClass("modal-overflow").attr("aria-hidden",true);e.support.transition&&this.$element.hasClass("fade")?this.hideWithTransition():this.hideModal()},layout:function(){var t=this.options.height?"height":"max-height",n=this.options.height||this.options.maxHeight;if(this.options.width){this.$element.css("width",this.options.width);var r=this;this.$element.css("margin-left",function(){if(/%/ig.test(r.options.width)){return-(parseInt(r.options.width)/2)+"%"}else{return-(e(this).width()/2)+"px"}})}else{this.$element.css("width","");this.$element.css("margin-left","")}this.$element.find(".modal-body").css("overflow","").css(t,"");if(n){this.$element.find(".modal-body").css("overflow","auto").css(t,n)}var i=e(window).height()-10<this.$element.height();if(i||this.options.modalOverflow){this.$element.css("margin-top",0).addClass("modal-overflow")}else{this.$element.css("margin-top",0-this.$element.height()/2).removeClass("modal-overflow")}},tab:function(){var t=this;if(this.isShown&&this.options.consumeTab){this.$element.on("keydown.tabindex.modal","[data-tabindex]",function(n){if(n.keyCode&&n.keyCode==9){var r=e(this),i=e(this);t.$element.find("[data-tabindex]:enabled:not([readonly])").each(function(t){if(!t.shiftKey){r=r.data("tabindex")<e(this).data("tabindex")?r=e(this):i=e(this)}else{r=r.data("tabindex")>e(this).data("tabindex")?r=e(this):i=e(this)}});r[0]!==e(this)[0]?r.focus():i.focus();n.preventDefault()}})}else if(!this.isShown){this.$element.off("keydown.tabindex.modal")}},escape:function(){var e=this;if(this.isShown&&this.options.keyboard){if(!this.$element.attr("tabindex"))this.$element.attr("tabindex",-1);this.$element.on("keyup.dismiss.modal",function(t){t.which==27&&e.hide()})}else if(!this.isShown){this.$element.off("keyup.dismiss.modal")}},hideWithTransition:function(){var t=this,n=setTimeout(function(){t.$element.off(e.support.transition.end);t.hideModal()},500);this.$element.one(e.support.transition.end,function(){clearTimeout(n);t.hideModal()})},hideModal:function(){var e=this.options.height?"height":"max-height";var t=this.options.height||this.options.maxHeight;if(t){this.$element.find(".modal-body").css("overflow","").css(e,"")}this.$element.hide().trigger("hidden")},removeLoading:function(){this.$loading.remove();this.$loading=null;this.isLoading=false},loading:function(t){t=t||function(){};var n=this.$element.hasClass("fade")?"fade":"";if(!this.isLoading){var r=e.support.transition&&n;this.$loading=e('<div class="loading-mask '+n+'">').append(this.options.spinner).appendTo(this.$element);if(r)this.$loading[0].offsetWidth;this.$loading.addClass("in");this.isLoading=true;r?this.$loading.one(e.support.transition.end,t):t()}else if(this.isLoading&&this.$loading){this.$loading.removeClass("in");var i=this;e.support.transition&&this.$element.hasClass("fade")?this.$loading.one(e.support.transition.end,function(){i.removeLoading()}):i.removeLoading()}else if(t){t(this.isLoading)}},focus:function(){var e=this.$element.find(this.options.focusOn);e=e.length?e:this.$element;e.focus()},attention:function(){if(this.options.attentionAnimation){this.$element.removeClass("animated").removeClass(this.options.attentionAnimation);var e=this;setTimeout(function(){e.$element.addClass("animated").addClass(e.options.attentionAnimation)},0)}this.focus()},destroy:function(){var t=e.Event("destroy");this.$element.trigger(t);if(t.isDefaultPrevented())return;this.teardown()},teardown:function(){if(!this.$parent.length){this.$element.remove();this.$element=null;return}if(this.$parent!==this.$element.parent()){this.$element.appendTo(this.$parent)}this.$element.off(".modal");this.$element.removeData("modal");this.$element.removeClass("in").attr("aria-hidden",true)}};e.fn.modal=function(n,r){return this.each(function(){var i=e(this),s=i.data("modal"),o=e.extend({},e.fn.modal.defaults,i.data(),typeof n=="object"&&n);if(!s)i.data("modal",s=new t(this,o));if(typeof n=="string")s[n].apply(s,[].concat(r));else if(o.show)s.show()})};e.fn.modal.defaults={keyboard:true,backdrop:true,loading:false,show:true,width:null,height:null,maxHeight:null,modalOverflow:false,consumeTab:true,focusOn:null,replace:false,resize:false,attentionAnimation:"shake",manager:"body",spinner:'<div class="loading-spinner" style="width: 200px; margin-left: -100px;"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div>',backdropTemplate:'<div class="modal-backdrop" />'};e.fn.modal.Constructor=t;e(function(){e(document).off("click.modal").on("click.modal.data-api",'[data-toggle="modal"]',function(t){var n=e(this),r=n.attr("href"),i=e(n.attr("data-target")||r&&r.replace(/.*(?=#[^\s]+$)/,"")),s=i.data("modal")?"toggle":e.extend({remote:!/#/.test(r)&&r},i.data(),n.data());t.preventDefault();i.modal(s).one("hide",function(){n.focus()})})})}(window.jQuery);!function(e){"use strict";function r(e){return function(t){if(this===t.target){return e.apply(this,arguments)}}}var t=function(e,t){this.init(e,t)};t.prototype={constructor:t,init:function(t,n){this.$element=e(t);this.options=e.extend({},e.fn.modalmanager.defaults,this.$element.data(),typeof n=="object"&&n);this.stack=[];this.backdropCount=0;if(this.options.resize){var r,i=this;e(window).on("resize.modal",function(){r&&clearTimeout(r);r=setTimeout(function(){for(var e=0;e<i.stack.length;e++){i.stack[e].isShown&&i.stack[e].layout()}},10)})}},createModal:function(t,n){e(t).modal(e.extend({manager:this},n))},appendModal:function(t){this.stack.push(t);var n=this;t.$element.on("show.modalmanager",r(function(r){var i=function(){t.isShown=true;var r=e.support.transition&&t.$element.hasClass("fade");n.$element.toggleClass("modal-open",n.hasOpenModal()).toggleClass("page-overflow",e(window).height()<n.$element.height());t.$parent=t.$element.parent();t.$container=n.createContainer(t);t.$element.appendTo(t.$container);n.backdrop(t,function(){t.$element.show();if(r){t.$element[0].offsetWidth}t.layout();t.$element.addClass("in").attr("aria-hidden",false);var i=function(){n.setFocus();t.$element.trigger("shown")};r?t.$element.one(e.support.transition.end,i):i()})};t.options.replace?n.replace(i):i()}));t.$element.on("hidden.modalmanager",r(function(r){n.backdrop(t);if(!t.$element.parent().length){n.destroyModal(t)}else if(t.$backdrop){var i=e.support.transition&&t.$element.hasClass("fade");if(i){t.$element[0].offsetWidth}e.support.transition&&t.$element.hasClass("fade")?t.$backdrop.one(e.support.transition.end,function(){t.destroy()}):t.destroy()}else{t.destroy()}}));t.$element.on("destroy.modalmanager",r(function(e){n.destroyModal(t)}))},getOpenModals:function(){var e=[];for(var t=0;t<this.stack.length;t++){if(this.stack[t].isShown)e.push(this.stack[t])}return e},hasOpenModal:function(){return this.getOpenModals().length>0},setFocus:function(){var e;for(var t=0;t<this.stack.length;t++){if(this.stack[t].isShown)e=this.stack[t]}if(!e)return;e.focus()},destroyModal:function(e){e.$element.off(".modalmanager");if(e.$backdrop)this.removeBackdrop(e);this.stack.splice(this.getIndexOfModal(e),1);var t=this.hasOpenModal();this.$element.toggleClass("modal-open",t);if(!t){this.$element.removeClass("page-overflow")}this.removeContainer(e);this.setFocus()},getModalAt:function(e){return this.stack[e]},getIndexOfModal:function(e){for(var t=0;t<this.stack.length;t++){if(e===this.stack[t])return t}},replace:function(t){var n;for(var i=0;i<this.stack.length;i++){if(this.stack[i].isShown)n=this.stack[i]}if(n){this.$backdropHandle=n.$backdrop;n.$backdrop=null;t&&n.$element.one("hidden",r(e.proxy(t,this)));n.hide()}else if(t){t()}},removeBackdrop:function(e){e.$backdrop.remove();e.$backdrop=null},createBackdrop:function(t,n){var r;if(!this.$backdropHandle){r=e(n).addClass(t).appendTo(this.$element)}else{r=this.$backdropHandle;r.off(".modalmanager");this.$backdropHandle=null;this.isLoading&&this.removeSpinner()}return r},removeContainer:function(e){e.$container.remove();e.$container=null},createContainer:function(t){var i;i=e('<div class="modal-scrollable">').css("z-index",n("modal",this.getOpenModals().length)).appendTo(this.$element);if(t&&t.options.backdrop!="static"){i.on("click.modal",r(function(e){t.hide()}))}else if(t){i.on("click.modal",r(function(e){t.attention()}))}return i},backdrop:function(t,r){var i=t.$element.hasClass("fade")?"fade":"",s=t.options.backdrop&&this.backdropCount<this.options.backdropLimit;if(t.isShown&&s){var o=e.support.transition&&i&&!this.$backdropHandle;t.$backdrop=this.createBackdrop(i,t.options.backdropTemplate);t.$backdrop.css("z-index",n("backdrop",this.getOpenModals().length));if(o)t.$backdrop[0].offsetWidth;t.$backdrop.addClass("in");this.backdropCount+=1;o?t.$backdrop.one(e.support.transition.end,r):r()}else if(!t.isShown&&t.$backdrop){t.$backdrop.removeClass("in");this.backdropCount-=1;var u=this;e.support.transition&&t.$element.hasClass("fade")?t.$backdrop.one(e.support.transition.end,function(){u.removeBackdrop(t)}):u.removeBackdrop(t)}else if(r){r()}},removeSpinner:function(){this.$spinner&&this.$spinner.remove();this.$spinner=null;this.isLoading=false},removeLoading:function(){this.$backdropHandle&&this.$backdropHandle.remove();this.$backdropHandle=null;this.removeSpinner()},loading:function(t){t=t||function(){};this.$element.toggleClass("modal-open",!this.isLoading||this.hasOpenModal()).toggleClass("page-overflow",e(window).height()<this.$element.height());if(!this.isLoading){this.$backdropHandle=this.createBackdrop("fade",this.options.backdropTemplate);this.$backdropHandle[0].offsetWidth;var r=this.getOpenModals();this.$backdropHandle.css("z-index",n("backdrop",r.length+1)).addClass("in");var i=e(this.options.spinner).css("z-index",n("modal",r.length+1)).appendTo(this.$element).addClass("in");this.$spinner=e(this.createContainer()).append(i).on("click.modalmanager",e.proxy(this.loading,this));this.isLoading=true;e.support.transition?this.$backdropHandle.one(e.support.transition.end,t):t()}else if(this.isLoading&&this.$backdropHandle){this.$backdropHandle.removeClass("in");var s=this;e.support.transition?this.$backdropHandle.one(e.support.transition.end,function(){s.removeLoading()}):s.removeLoading()}else if(t){t(this.isLoading)}}};var n=function(){var t,n={};return function(r,i){if(typeof t==="undefined"){var s=e('<div class="modal hide" />').appendTo("body"),o=e('<div class="modal-backdrop hide" />').appendTo("body");n["modal"]=+s.css("z-index");n["backdrop"]=+o.css("z-index");t=n["modal"]-n["backdrop"];s.remove();o.remove();o=s=null}return n[r]+t*i}}();e.fn.modalmanager=function(n,r){return this.each(function(){var i=e(this),s=i.data("modalmanager");if(!s)i.data("modalmanager",s=new t(this,n));if(typeof n==="string")s[n].apply(s,[].concat(r))})};e.fn.modalmanager.defaults={backdropLimit:999,resize:true,spinner:'<div class="loading-spinner fade" style="width: 200px; margin-left: -100px;"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div>',backdropTemplate:'<div class="modal-backdrop" />'};e.fn.modalmanager.Constructor=t;e(function(){e(document).off("show.bs.modal").off("hidden.bs.modal")})}(jQuery)       

 /* Submit button Disabled until Validation */

  $(document).ready(function() {
    validate();
    $('#inputName, #inputEmail, #inputMessage' ).change(validate);
  });

  function validate(){
      if ($('#inputName').val().length   >   0   &&
          $('#inputEmail').val().length  >   0   &&
          $('#inputMessage').val().length    >   0) {
          $("input[type=submit]").prop("disabled", false);
      }
      else {
        $("input[type=submit]").prop("disabled", true);
      }
  }

/* Honey Trap */
$(document).ready(function (){
  $('.inputGhost').on('change', function() {
    $("input[type=submit]").prop('disabled', $(this).val().length > 0);
  });
});
