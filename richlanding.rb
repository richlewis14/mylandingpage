require 'bundler/setup'
Bundler.require(:default)
require './config/config.rb' if File.exists?('./config/config.rb')
require 'rubygems'
require 'sinatra'
require 'sinatra/static_assets'
require 'pony'
configure :production do
  require 'newrelic_rpm'
end

get '/' do
  erb :index
end

post '/' do
    from = params[:name]
    subject = "#{params[:name]} has contacted you from your Personal Website"
    body = erb(:mail, layout: false)

  Pony.mail(
  :from => from,
  :to => ENV["EMAIL_ADDRESS"],
  :subject => subject,
  :body => body,
  :via => :smtp,
  :via_options => {
    :address              => 'smtp.gmail.com',
    :port                 => '587',
    :enable_starttls_auto => true,
    :user_name            => ENV["USER_NAME"],
    :password             => ENV["PASSWORD"],
    :authentication       => :plain, 
    :domain               => "localhost.localdomain" 
})
  redirect '/success' 

end

get '/success' do
  erb :thankyou
end
